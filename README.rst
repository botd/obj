README
======

OBJ is a pure python3 framework that allows storage of JSON object to a filestamped file on the disk. 
OBJ uses a timestamped, type in filename, JSON stringified, files on filesystem backend and has timed based logging capabilities. 
OBJ has been placed in the Public Domain and contains no copyright or LICENSE.

you can contact me on IRC/freenode/#dunkbots.

| Bart Thate (bthate@dds.nl, thatebart@gmail.com)
| botfather on #dunkbots irc.freenode.net

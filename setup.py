# setup.py

from setuptools import setup, find_namespace_packages

setup(
    name='obj',
    version='125',
    url='https://bitbucket.org/bthate/obj',
    author='Bart Thate',
    author_email='bthate@dds.nl',
    description=""" OBJ is a library for managing objects and basic program functionality. """,
    long_description="""
    OBJ is a pure python3 framework that allows storage of JSON object to a filestamped file on the disk. 
    OBJ uses a timestamped, type in filename, JSON stringified, files on filesystem backend and has timed based logging capabilities. 
    OBJ has been placed in the Public Domain and contains no copyright or LICENSE.
 
    
    have fun coding ;]


    you can contact me on IRC/freenode/#dunkbots.

    | Bart Thate (bthate@dds.nl, thatebart@gmail.com)
    | botfather on #dunkbots irc.freenode.net

    """,
    long_description_content_type="text/x-rst",
    license='Public Domain',
    zip_safe=True,
    py_modules=["obj", "hdl", "shl", "thr", "tms"],
    classifiers=['Development Status :: 3 - Alpha',
                 'License :: Public Domain',
                 'Operating System :: Unix',
                 'Programming Language :: Python',
                 'Topic :: Utilities'
                ]
)
